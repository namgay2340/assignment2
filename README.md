Assignment 2
Data Science Technology and Systems PG (11523)
Namgay Tshering
u3246004

# Flight Delay Prediction

This repository contains a machine learning model for predicting flight delays based on weather data. The model is designed to help users to predict a flight being delayed due to weather conditions. 

Python libraries included or import as below:

```
import os
from pathlib2 import Path
from zipfile import ZipFile
import time
import pandas as pd
import numpy as np
import subprocess
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_curve, roc_auc_score, auc
```


Below are instructions on how to run the code:

To add remote use below link: <br/>
git remote add origin https://gitlab.com/namgay2340/assignment2.git <br/>

To clone remote use below link: <br/>
git clone origin https://gitlab.com/namgay2340/assignment2.git

- Python (version 3.11)
git branch -M master
git push -uf origin master
